package com.amirmohammadazimi.calculator;

import android.view.View;
import android.widget.TextView;

/**
 * Created by Amir Mohammad Azimi on 4/4/2017.
 */

public class Operations implements View.OnClickListener {

    public static  double a;
    public static double b;
    public static int op;

    TextView textView;
    TextView textView2;
    TextView textView3;


    public Operations(TextView textView, TextView textView2 ,TextView textView3 ){
        a =0;
        b = 0;
        op = 1;
        this.textView=textView;
        this.textView2 = textView2;
        this.textView3 = textView3;

    }

    @Override
    public void onClick(View v) {


        b = Double.parseDouble(textView.getText().toString());



        try{
            if(op==1){
                a = a+b;
            }else if (op==2){
                a = a-b;
            }else if (op==3){
                a = a*b;

            }else if (op==4){
                a=a/b;
            }
        }catch (ArithmeticException e){

        }


        textView.setText("0");
        textView2.setText(String.valueOf(a));

        if(v.getId() == R.id.buttonSum){
            op=1;
            textView3.setText("+");
        }else if(v.getId() == R.id.buttonMinus){
            op=2;
            textView3.setText("-");
        }else if(v.getId() == R.id.buttonProduct){
            op=3;
            textView3.setText("*");
        }else if(v.getId() == R.id.buttonDivide){
            op=4;
            textView3.setText("/");
        }else if(v.getId() == R.id.imageButton){
            op=1;
            a=0;
            b=0;
            textView.setText(String.valueOf(0));
            textView2.setText(String.valueOf(0));
        }else if (v.getId() == R.id.buttonEqual){
            textView3.setText("=");

        }





    }
}
