package com.amirmohammadazimi.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView, t1;
    Button b1 , b2, b3 , b4 , b5 , b6 , b7 , b8 , b9 , b0 , bSum , bProduct , bDivide , bMinus , bEq, bDot ;
    ImageButton back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textMain);
        t1 =(TextView) findViewById(R.id.text1) ;
        Operations op = new Operations(textView , (TextView) findViewById(R.id.textView ),t1);
        Adding_To_TextView ATT = new Adding_To_TextView(textView);

        b1 = (Button) findViewById(R.id.button1);
        b2 = (Button) findViewById(R.id.button2);
        b3 = (Button) findViewById(R.id.button3);
        b4 = (Button) findViewById(R.id.button4);
        b5 = (Button) findViewById(R.id.button5);
        b6 = (Button) findViewById(R.id.button6);
        b7 = (Button) findViewById(R.id.button7);
        b8 = (Button) findViewById(R.id.button8);
        b9 = (Button) findViewById(R.id.button9);
        b0 = (Button) findViewById(R.id.button0);


        bSum = (Button) findViewById(R.id.buttonSum);
        bProduct = (Button) findViewById(R.id.buttonProduct);
        bDivide = (Button) findViewById(R.id.buttonDivide);
        bMinus = (Button) findViewById(R.id.buttonMinus);

        bEq = (Button) findViewById(R.id.buttonEqual);

        bDot = (Button) findViewById(R.id.buttonDot);

        back = (ImageButton) findViewById(R.id.imageButton);

        b1.setOnClickListener(ATT);
        b2.setOnClickListener(ATT);
        b3.setOnClickListener(ATT);
        b4.setOnClickListener(ATT);
        b5.setOnClickListener(ATT);
        b6.setOnClickListener(ATT);
        b7.setOnClickListener(ATT);
        b8.setOnClickListener(ATT);
        b9.setOnClickListener(ATT);
        b0.setOnClickListener(ATT);



        bDot.setOnClickListener(ATT);





        bSum.setOnClickListener(op);
        bMinus.setOnClickListener(op);
        bProduct.setOnClickListener(op);
        bDivide.setOnClickListener(op);

        bEq.setOnClickListener(op);


        back.setOnClickListener(op);






    }

}
