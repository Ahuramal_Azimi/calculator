package com.amirmohammadazimi.calculator;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Amir Mohammad Azimi on 4/4/2017.
 */

public class Adding_To_TextView implements View.OnClickListener {

    TextView textView;

    public Adding_To_TextView(TextView textView){

        this.textView = textView;
        textView.setText("0");

    }



    @Override
    public void onClick(View v) {

        textView.append(((Button)(v)).getText().toString());

    }
}
