package com.amirmohammadazimi.calculator;

import android.view.View;
import android.widget.TextView;

/**
 * Created by Amir Mohammad Azimi on 4/4/2017.
 */
/**
*THIS IS NOTHING
 */
public class Equal implements View.OnClickListener {

    TextView textView;
    TextView textViewMain;



    @Override
    public void onClick(View v) {

    }



    public  int operation(int num1 , int num2 , int num3 , int num4 , String op1, String op2 , String op3){


        if (op1.trim().equals("+")){

            if (op2.trim().equals("+")){

                if (op3.trim().equals("+")){
                    return num1+num2+num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1+num2+num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1+num2+num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1+num2+num3/num4;
                }

            }else if (op2.trim().equals("-")){

                if (op3.trim().equals("+")){
                    return num1+num2-num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1+num2-num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1+num2-num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1+num2-num3/num4;
                }

            }else if (op2.trim().equals("*")){

                if (op3.trim().equals("+")){
                    return num1+num2*num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1+num2*num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1+num2*num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1+num2*num3/num4;
                }

            }else if (op2.trim().equals("/")){

                if (op3.trim().equals("+")){
                    return num1+num2/num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1+num2/num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1+num2/num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1+num2/num3/num4;
                }

            }

        }else if (op1.trim().equals("-")){

            if (op2.trim().equals("+")){

                if (op3.trim().equals("+")){
                    return num1-num2+num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1-num2+num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1-num2+num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1-num2+num3/num4;
                }

            }else if (op2.trim().equals("-")){

                if (op3.trim().equals("+")){
                    return num1-num2-num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1-num2-num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1-num2-num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1-num2-num3/num4;
                }

            }else if (op2.trim().equals("*")){

                if (op3.trim().equals("+")){
                    return num1-num2*num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1-num2*num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1-num2*num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1-num2*num3/num4;
                }

            }else if (op2.trim().equals("/")){

                if (op3.trim().equals("+")){
                    return num1-num2/num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1-num2/num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1-num2/num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1-num2/num3/num4;
                }

            }

        }else if (op1.trim().equals("*")){

            if (op2.trim().equals("+")){

                if (op3.trim().equals("+")){
                    return num1*num2+num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1*num2+num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1*num2+num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1*num2+num3/num4;
                }

            }else if (op2.trim().equals("-")){

                if (op3.trim().equals("+")){
                    return num1*num2-num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1*num2-num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1*num2-num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1*num2-num3/num4;
                }

            }else if (op2.trim().equals("*")){

                if (op3.trim().equals("+")){
                    return num1*num2*num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1*num2*num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1*num2*num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1*num2*num3/num4;
                }

            }else if (op2.trim().equals("/")){

                if (op3.trim().equals("+")){
                    return num1*num2/num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1*num2/num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1*num2/num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1*num2/num3/num4;
                }

            }

        }else if (op1.trim().equals("/")){
            if (op2.trim().equals("+")){

                if (op3.trim().equals("+")){
                    return num1/num2+num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1/num2+num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1/num2+num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1/num2+num3/num4;
                }

            }else if (op2.trim().equals("-")){

                if (op3.trim().equals("+")){
                    return num1/num2-num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1/num2-num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1/num2-num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1/num2-num3/num4;
                }

            }else if (op2.trim().equals("*")){

                if (op3.trim().equals("+")){
                    return num1/num2*num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1/num2*num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1/num2*num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1/num2*num3/num4;
                }

            }else if (op2.trim().equals("/")){

                if (op3.trim().equals("+")){
                    return num1/num2/num3+num4;
                }else if (op3.trim().equals("-")){
                    return num1/num2/num3-num4;
                }else if (op3.trim().equals("*")){
                    return num1/num2/num3*num4;
                }else if (op3.trim().equals("/")){
                    return num1/num2/num3/num4;
                }

            }

        }
        return 0;
    }
}
