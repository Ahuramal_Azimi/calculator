package com.amirmohammadazimi.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondLayout extends AppCompatActivity {
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_layout);


        text = (TextView) findViewById(R.id.textView);

        Intent intent = getIntent();

        text.setText("Hello Mr. "+intent.getStringExtra("name")+"  "+intent.getStringExtra("lastName")+" "
                +"You are "+intent.getStringExtra("age")+" Years old");

    }
}
