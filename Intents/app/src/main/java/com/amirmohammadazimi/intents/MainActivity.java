package com.amirmohammadazimi.intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText name, lastName, age;
    Button go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.editTextN);
        lastName = (EditText) findViewById(R.id.editTextF);
        age = (EditText) findViewById(R.id.editTextA);


        go = (Button) findViewById(R.id.Go);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(MainActivity.this, SecondLayout.class);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("lastName", lastName.getText().toString());
                intent.putExtra("age", age.getText().toString());

                startActivity(intent);
            }
        });


    }
}
